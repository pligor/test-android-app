package com.pligor.testapp.adapters

import android.content.Context
import android.view.{ViewGroup, View}
import android.widget.{TextView, ImageView, ArrayAdapter}
import com.pligor.testapp.R
import scala.collection.JavaConverters._
import scala.collection.SeqLike

/**
 * Created by pligor on 6/17/15.
 */
object IconicAdapter {
  private class ViewHolder(row: View) {
    val iconImageView = row.findViewById(R.id.rowIcon).asInstanceOf[ImageView]

    val sizeTextView = row.findViewById(R.id.rowSizeTextView).asInstanceOf[TextView]
  }
}

class IconicAdapter(listOfItems: List[String])(implicit context: Context)
  extends ArrayAdapter[String](context, R.layout.row, R.id.rowLabel, listOfItems.asJava) {
  import IconicAdapter._

  override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
    val row = super.getView(position, convertView, parent)

    val viewHolder = Option(row.getTag).getOrElse({
      val newViewHolder = new ViewHolder(row)
      row.setTag(newViewHolder)
      newViewHolder
    }).asInstanceOf[ViewHolder]

    val curLen = listOfItems(position).length

    viewHolder.iconImageView.setImageResource(
      if (curLen >= 4) {
        R.drawable.facebook
      } else {
        R.drawable.twitter
      }
    )

    viewHolder.sizeTextView.setText(curLen.toString)

    row
  }
}
