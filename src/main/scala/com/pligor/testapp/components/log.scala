package com.pligor.testapp.components

import com.pligor.myandroid.logObject
import com.pligor.testapp.TestApp

/**
 * Created by pligor on 6/27/15.
 */
object log extends logObject {
  override protected def isDebuggable: Boolean = TestApp.isDebuggable
}
