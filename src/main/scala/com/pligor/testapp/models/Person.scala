package com.pligor.testapp.models

import android.os.{Parcelable, Parcel}
import android.os.Parcelable.Creator

/**
 * Created by pligor on 6/27/15.
 */
object Person {
  final val CREATOR: Creator[Person] = new Creator[Person] {
    override def newArray(size: Int): Array[Person] = {
      new Array[Person](size)
    }

    override def createFromParcel(parcel: Parcel): Person = new Person(
      name = parcel.readString(),
      age = parcel.readInt()
    )
  }
}

class Person(val name: String, val age: Int) extends Parcelable {
  override def writeToParcel(parcel: Parcel, flags: Int): Unit = {
    parcel.writeString(name)

    parcel.writeInt(age)
  }

  override def describeContents(): Int = 0;	//use CONTENTS_FILE_DESCRIPTOR if you are passing a file descriptor
}
