package com.pligor.testapp.models

import android.content.{ContentValues, Context}
import com.pligor.myandroid.sqlite.{DatabaseHandlerClass, DatabaseHandlerObject, MySQLiteOpenHelperSecure}
import net.sqlcipher.database.SQLiteDatabase

//import android.database.sqlite.SQLiteDatabase

object DatabaseHandler extends DatabaseHandlerObject[SQLiteDatabase] {
  protected val DATABASE_NAME = "testapp"

  protected val DATABASE_VERSION = 1

  protected val DB_PASS = """11111111112222222222333333333344444444445555555555666666666677777777778888888888"""

  override protected def generateInstance(implicit context: Context): DatabaseHandler = new DatabaseHandler()
}

/**
 * We create this object for the specific database, NOT for each table
 */
class DatabaseHandler(implicit context: Context) extends MySQLiteOpenHelperSecure(
  "Fcarrier.FULLPATH_DIRECTORY", //TODO
  DatabaseHandler.DATABASE_NAME,
  DatabaseHandler.DATABASE_VERSION,
  dbPass = DatabaseHandler.DB_PASS,
  creator = (db: SQLiteDatabase) => {} //TableCreator.apply
) with DatabaseHandlerClass[SQLiteDatabase] {

  def populate(implicit context: Context): Unit = {
    throw new Exception("IMPLEMENT")
    reset()
    //TablePopulator(insert);
  }

  protected def populator(insert: (String, ContentValues) => Long)
                         (implicit context: Context): Unit = {
    //TestingTablePopulator(insert);
  }

  override def doOnUpgrade(upgradeTo: Int)(implicit db: SQLiteDatabase): Unit = {
    upgradeTo match {
      case 2 =>
      //Some changes in the database here: PhoneCall.createTable(db)
      case 3 =>
      //...
    }
  }
}
