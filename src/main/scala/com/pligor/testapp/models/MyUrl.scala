package com.pligor.testapp.models

import com.pligor.myandroid.parcel.{ObjectCaseClassParcelable, CaseClassParcelableTrait}

/**
 * Created by pligor on 6/27/15
 */
object MyUrl extends ObjectCaseClassParcelable[MyUrl] {
  protected def getClassReference: Class[MyUrl] = classOf[MyUrl]
}

case class MyUrl(urlStr: String, dummy: Long) extends CaseClassParcelableTrait {

}

