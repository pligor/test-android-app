package com.pligor.testapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.ImageButton
import com.pligor.testapp.R
import com.pligor.testapp.models.{MyUrl, Person}
import org.scaloid.common.SActivity

/**
 * Created by pligor on 6/26/15.
 */
trait TintingActivity extends SActivity {
  private lazy val whiteFaceImageButton = findViewById(R.id.whiteFaceImageButton).asInstanceOf[ImageButton]

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.tinting)

    whiteFaceImageButton.setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        startActivity(
          new Intent(ctx, classOf[TextActivity]).putExtras({
            val bundle = new Bundle()

            bundle.putParcelable(TextActivity.EXTRAS.URL_MESSAGE.toString, MyUrl("http://stackoverflow.com", 19L))

            bundle
          })
        )
      }
    })
  }


}
