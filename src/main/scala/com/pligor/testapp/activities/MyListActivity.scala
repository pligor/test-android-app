package com.pligor.testapp.activities

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.widget.ListView
import com.pligor.testapp.R
import com.pligor.testapp.adapters.IconicAdapter

import scala.util.Random

/**
 * Created by pligor on 6/17/15.
 */
object MyListActivity {
  private val items = List.range(0, 100).map(x => {
    Random.alphanumeric.take(Random.nextInt(10) + 2).mkString
  })
}

class MyListActivity extends Activity {

  import MyListActivity._

  private lazy val singleListView = findViewById(R.id.singleListView).asInstanceOf[ListView]

  implicit val ctx: Context = this

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.simplelistview)

    singleListView.setAdapter(
      new IconicAdapter(items)
    )
  }
}
