package com.pligor.testapp.activities

import java.util

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.{MenuItem, Menu, View}
import android.view.View.OnClickListener
import android.widget.{Toast, ListView, ArrayAdapter}
import com.pligor.mySplunkMint.SplunkMintSActivity
import com.pligor.testapp.R
import com.splunk.mint.Mint
import org.scaloid.common.SActivity
import scala.collection.JavaConverters._

/**
 * Created by pligor on 6/18/15.
 */
object StylingActivity {
  val items = List[String]("one", "two", "three", "four", "six", "ten", "twenty","one hundred")
}

trait StylingActivity extends SActivity with SplunkMintSActivity {

  import StylingActivity._

  private lazy val resettableListView = findViewById(R.id.resettableListView).asInstanceOf[ListView]

  override protected def isSplunkMintEnabled: Boolean = false

  override protected def SPLUNK_MINT_API_KEY: String = "d374e0bb"

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.styling)

    findViewById(R.id.styledButton).setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        Mint.logEvent("styled_button_clicked")
      }
    })

    //getActionBar.setHomeButtonEnabled(true) //home button is the button of the action bar, not the home button of the overall cellphone

    getActionBar.setDisplayShowHomeEnabled(true)  //defaults to the icon in manifest
    getActionBar.setIcon(R.drawable.twitter)  //here we override the icon

    initAdapter()
  }

  private def initAdapter() {
    //val words = new Array[String](items.length)

    //items.take(3).copyToArray(words)

    //val words = List[String]().asJava

    //words.addAll(items.take(3).asJavaCollection)

    val words = new util.ArrayList[String]( util.Arrays.asList( items.take(3): _ * ) )  //:P to JAVA

    val adapter = new ArrayAdapter[String](this, android.R.layout.simple_list_item_1, words)

    resettableListView.setAdapter(adapter)
  }

  override def onCreateOptionsMenu(menu: Menu): Boolean = {
    getMenuInflater.inflate(R.menu.actions, menu)

    super.onCreateOptionsMenu(menu)
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.addMenuItem =>
        Log.i("pligor", "adding something")
        resettableListView.getAdapter.asInstanceOf[ArrayAdapter[String]].add(
          items(resettableListView.getAdapter.getCount)
        )

      case R.id.resetMenuItem =>
        Log.i("pligor", "resetting something")
        initAdapter()

      case R.id.aboutMenuItem =>
        Log.i("pligor", "showing some about screen")
        Toast.makeText(this, "about us you will NOT know", Toast.LENGTH_LONG).show()

      //case R.id.home =>
    }

    super.onOptionsItemSelected(item)
  }
}
