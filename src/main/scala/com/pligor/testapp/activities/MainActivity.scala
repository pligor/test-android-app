package com.pligor.testapp.activities

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import android.widget.AdapterView.{OnItemSelectedListener, OnItemClickListener}
import android.widget.CompoundButton.OnCheckedChangeListener
import android.widget._
import com.pligor.testapp.R
import scala.collection.JavaConverters._

/**
 * Created by pligor on 6/12/15.
 */

object MainActivity {
  private val items = List[String](
    "orem", "ipsum", "dolor", "sit", "amet", "consectetuer", "adipiscing", "elit", "morbi", "vel", "ligula", "vitae", "arcu",
    "aliquet", "mollis", "etiam", "vel", "erat", "placerat", "ante", "porttitor", "sodales", "pellentesque", "augue", "purus"
  )

  trait Spinned extends OnItemClickListener {
    override def onItemClick(parent: AdapterView[_], view: View, position: Int, id: Long): Unit = {
      Log.i("pligor", "we have been clicked baby")
    }
  }
}

class MainActivity extends Activity {
  //private lazy val progressBar1 = findViewById(R.id.progressBar1).asInstanceOf[ProgressBar]

  private lazy val mySpinner = findViewById(R.id.mySpinner).asInstanceOf[Spinner]

  private lazy val myListView = findViewById(R.id.myListView).asInstanceOf[ListView]
  private lazy val multiListView = findViewById(R.id.multiListView).asInstanceOf[ListView]
  private lazy val curSelectionTextView = findViewById(R.id.curSelectionTextView).asInstanceOf[TextView]

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    import MainActivity._

    setContentView(
      //R.layout.main
      //R.layout.progressbar
      R.layout.listview
    )

    myListView.setAdapter(
      new ArrayAdapter[String](this, android.R.layout.simple_list_item_single_choice, items.asJava)
    )

    myListView.setOnItemClickListener(new OnItemClickListener with Spinned {
      override def onItemClick(parent: AdapterView[_], view: View, position: Int, id: Long): Unit = {
        super.onItemClick(parent, view, position, id)

        assert(position == id)

        curSelectionTextView.setText(items(position))
      }
    })

    multiListView.setAdapter(
      new ArrayAdapter[String](this, android.R.layout.simple_list_item_multiple_choice, items.asJava)
    )


    val adapter = new ArrayAdapter[String](this, android.R.layout.simple_spinner_item, items.asJava)

    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

    mySpinner.setAdapter(
      adapter
    )

    mySpinner.setOnItemSelectedListener(new OnItemSelectedListener {
      override def onNothingSelected(parent: AdapterView[_]): Unit = {
        Toast.makeText(MainActivity.this, "Select something you mad", Toast.LENGTH_LONG)
      }

      override def onItemSelected(parent: AdapterView[_], view: View, position: Int, id: Long): Unit = {
        curSelectionTextView.setText(items(position))
      }
    })

    /*Log.i(
      "pligor",
      progressBar1.isIndeterminate.toString
    )*/

    /*findViewById(R.id.hitButton).setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        Log.i("pligor", "button was hit")
      }
    })

    findViewById(R.id.mycheck).asInstanceOf[CheckBox].setOnCheckedChangeListener(new OnCheckedChangeListener {
      override def onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean): Unit = {
        if(isChecked) {
          Log.i("pligor", "now is ok")
        } else {
          Log.i("pligor", "now is NOT ok")
        }
      }
    })*/
  }
}
