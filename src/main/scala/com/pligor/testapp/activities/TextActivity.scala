package com.pligor.testapp.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import com.pligor.testapp.R
import com.pligor.testapp.models.MyUrl

/**
 * Created by pligor on 6/26/15
 */
object TextActivity {

  object EXTRAS extends Enumeration {
    val URL_MESSAGE = Value
  }

}

class TextActivity extends Activity {

  import TextActivity._

  private lazy val inGrButton = findViewById(R.id.inGrButton).asInstanceOf[Button]

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.simpletext)

    inGrButton.setOnClickListener(new OnClickListener {
      override def onClick(v: View): Unit = {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
          //getIntent.getStringExtra(EXTRAS.URL_MESSAGE.toString)
          getIntent.getExtras.getParcelable[MyUrl](EXTRAS.URL_MESSAGE.toString).urlStr
        )))
      }
    })
  }
}
