package com.pligor.testapp.activities

import android.app.Activity
import android.os.{PersistableBundle, Bundle}
import android.util.Log
import android.widget.{AutoCompleteTextView, ArrayAdapter, TextView, GridView}
import com.pligor.testapp.R
import scala.collection.JavaConverters._

/**
 * Created by pligor on 6/17/15.
 */
object MyGridActivity {
  private val items = List[String](
    "orem", "ipsum", "dolor", "sit", "amet", "consectetuer", "adipiscing", "elit", "morbi", "vel", "ligula", "vitae", "arcu",
    "aliquet", "mollis", "etiam", "vel", "erat", "placerat", "ante", "porttitor", "sodales", "pellentesque", "augue", "purus"
  )
}

class MyGridActivity extends Activity {

  import MyGridActivity._

  private lazy val myautocomple = findViewById(R.id.myautocomple).asInstanceOf[AutoCompleteTextView]

  private lazy val myGridView = findViewById(R.id.myGridView).asInstanceOf[GridView]

  private lazy val gridSelectionTextView = findViewById(R.id.gridSelectionTextView).asInstanceOf[TextView]

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.gridview)

    setTitle("Grid View")

    myGridView.setAdapter(
      new ArrayAdapter[String](this, R.layout.cell, items.asJava)
    )

    myautocomple.setAdapter(
      new ArrayAdapter[String](this, android.R.layout.simple_dropdown_item_1line, items.asJava)
    )
  }
}
