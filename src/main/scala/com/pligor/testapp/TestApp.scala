package com.pligor.testapp

import android.app.Application
import android.content.{Intent, Context}
import com.pligor.myandroid.sqlite.MySQLiteOpenHelper

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * NO WE ARE NOT SAVING DATA FOR PHONE NUMBERs THAT DO NOT CORRESPOND TO CONTACTS. THIS IS SILLY
 */
object TestApp {
  //for social login use this: http://code.google.com/p/socialauth-android/w/list

  //TODO make this false for production
  val isDebuggable = true

  //TODO val SERVER_HOSTNAME = "http://fcarrier.dnsdynamic.com"

  val ADMIN_CONTACT = "george@pligor.com"

  val GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id=com.pligor.testapp"

  val PREFERENCES_KEY = "XXXXXXXXXXXXXXXX" //16 in length because of 128 bit encryption key

  //TODO private val SUBPATH_DIRECTORY = "testapp"
  //TODO val FULLPATH_DIRECTORY = sdSubpath2fullpath(SUBPATH_DIRECTORY)

  //TODO val BUGSENSE_API_KEY = "XXXXXXXXXXX"

  /**
   * http://mobile.tutsplus.com/tutorials/android/android-sdk-implement-a-share-intent/
   */
  /*def getShareIntent(implicit context: Context) = {
    Intent.createChooser(
      new Intent(android.content.Intent.ACTION_SEND).
        setType(ContentType.TEXT_PLAIN.toString).
        putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject)).
        putExtra(android.content.Intent.EXTRA_TEXT, GOOGLE_PLAY_URL)
      , context.getString(R.string.share_title)
    )
  }*/
}

class TestApp extends Application {
  override def onCreate(): Unit = {
    super.onCreate()

    MySQLiteOpenHelper.loadSqlcipherLibs(this)
  }

  override def onTerminate(): Unit = {
    super.onTerminate()
  }
}