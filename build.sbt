useProguard in Android := true

javacOptions ++= Seq("-source", "1.7", "-target", "1.7")  //enable if issues occur

//android.Plugin.androidBuild //do NOT use this if you have autobuild enabled (see build.scala)

// pick the version of scala you want to use
scalaVersion := "2.11.6"

scalacOptions in Compile ++= Seq("-deprecation", "-unchecked", "-feature", "-Xlint", "-language:postfixOps", "-language:implicitConversions", "-encoding", "UTF-8")

dexMaxHeap in Android := "4096m"  //to avoid getting out of memory exception

typedResources in Android := false

transitiveAndroidLibs in Android := false //will not be able to load libraries from aars recursively

proguardScala in Android := true

//dexCoreLibrary in Android := true

//EXCLUDES
apkbuildExcludes in Android += "META-INF/services/com.fasterxml.jackson.databind.Module"

apkbuildExcludes in Android += "META-INF/DEPENDENCIES"

apkbuildExcludes in Android += "META-INF/DEPENDENCIES.txt"

apkbuildExcludes in Android += "META-INF/dependencies.txt"

apkbuildExcludes in Android += "META-INF/LICENSE"

apkbuildExcludes in Android += "META-INF/LICENSE.txt"

apkbuildExcludes in Android += "META-INF/license.txt"

apkbuildExcludes in Android += "META-INF/NOTICE"

apkbuildExcludes in Android += "META-INF/NOTICE.txt"

apkbuildExcludes in Android += "META-INF/notice.txt"

apkbuildExcludes in Android += "META-INF/ASL2.0"

apkbuildExcludes in Android += "META-INF/LGPL2.1"


//PROGUARD CACHES///////////////////////////////////////////////////////////////////////////////////
//INSTRUCTIONS HERE: https://github.com/pfn/android-sdk-plugin/issues/64
//proguardCache in Android += ProguardCache("package1", "package2", "all.root.packages") % "groupid" % "artifactid" (%% before artifact id is also supported)
//if a package is already contained in another jar then you have to enter a subpackage
//I mean if "alpha" causes exception use "alpha.beta" (can be more elaborate if there are many packages, but c'est la vie)

//these are causing unknown to me issues
//proguardCache in Android += ProguardCache("org.spongycastle") % "com.madgag" % "scpg-jdk15on"
//proguardCache in Android += ProguardCache("android.support.v4") % "android.support.v4"
//proguardCache in Android += ProguardCache("org.apache") % "org.apache"
//proguardCache in Android += ProguardCache("com.fasterxml.jackson") % "io.backchat.jerkson"

//DEPRECATED ProguardCache, proguardCache in Android += ProguardCache("org.scaloid.common") % "org.scaloid" %% "scaloid"
proguardCache in Android += "org.scaloid" //package prefix strings

proguardCache in Android += "org.apache"

proguardCache in Android += "play.api"

proguardCache in Android += "org.scalaj"

//RESOLVERS/////////////////////////////////////////////////////////////////////////////////////////

//Resolver.file("Local Repository", file("/home/weakwire/.ivy2/cache/"))(Resolver.ivyStylePatterns)
//"Big Bee Consultants" at "http://repo.bigbeeconsultants.co.uk/repo",
//resolvers += "Mandubian repository releases" at "https://github.com/mandubian/mandubian-mvn/raw/master/releases/"
//resolvers += "Splunk MINT repo" at "https://mint.splunk.com/maven/"

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

//resolvers += "Commonsware Safe" at "https://repo.commonsware.com.s3.amazonaws.com"
resolvers += "Commonsware" at "http://repo.commonsware.com"

//resolvers += Resolver.file("Android Extras", file("/home/pligor/android-sdk-linux/extras/android/m2repository/com/android/support/support-v13/22.2.0/"))(Resolver.ivyStylePatterns) //for local aar

//LIBRARY DEPENDENCIES///////////////////////////////////////////////////////////////////////////////////////
//libraryDependencies += aar("com.android.support" % "support-v13" % "22.2.0")  //local aar
//libraryDependencies += "com.android.support" % "support-v13" % "22.2.0"  //local aar

//libraryDependencies += "com.splunk.mint" % "mint" % "4.2.1" //is not working

//libraryDependencies += "commons-codec" % "commons-codec" % "1.10"

libraryDependencies += "org.scalaj" %% "scalaj-http" % "1.1.4" exclude("junit", "junit")

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.4.1"

libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5"

libraryDependencies += "org.scaloid" %% "scaloid" % "4.0-RC1"

libraryDependencies += aar("com.commonsware.cwac" % "colormixer" % "0.6.1") //https://github.com/commonsguy/cwac-colormixer
//libraryDependencies += "com.commonsware.cwac" % "colormixer" % "0.6.1" //this works as well

